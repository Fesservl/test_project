const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const PATHS = {
	source: path.join(__dirname, 'source'),
	build: path.join(__dirname, 'build')
};
module.exports = {
	entry: PATHS.source + '/index.js',
	output: {path: PATHS.build, filename: '[name].js'},
	plugins: [new HtmlWebpackPlugin({title: 'PhoneNumbers'})],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['env', 'stage-0']
					}
				}
			},
			{test: /\.less$/, use: ['style-loader', 'css-loader', 'less-loader']},
			{test: /\.(jpg|png|svg)$/, loader: 'file-loader', options: {name: 'images/[name].[ext]'}}
		]
	},
	devServer: {
		stats: 'errors-only',
		inline: true,
		port: 7777
	}
};
